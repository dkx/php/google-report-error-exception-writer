<?php

declare(strict_types=1);

namespace DKX\GoogleReportErrorExceptionWriter;

use Google\Cloud\ErrorReporting\V1beta1\ErrorContext;
use Google\Cloud\ErrorReporting\V1beta1\HttpRequestContext;
use Google\Cloud\ErrorReporting\V1beta1\ReportedErrorEvent;
use Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient;
use Google\Cloud\ErrorReporting\V1beta1\ServiceContext;
use Google\Cloud\ErrorReporting\V1beta1\SourceLocation;
use Psr\Http\Message\ServerRequestInterface;

final class ReportErrorExceptionWriter
{
	/** @var \Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient */
	private $client;

	/** @var string */
	private $googleProjectId;

	/** @var string */
	private $projectName;

	/** @var string|null */
	private $projectVersion;

	public function __construct(
		ReportErrorsServiceClient $client,
		string $googleProjectId,
		string $projectName,
		?string $projectVersion = null
	) {
		$this->client = $client;
		$this->googleProjectId = $googleProjectId;
		$this->projectName = $projectName;
		$this->projectVersion = $projectVersion;
	}

	public function writeException(\Throwable $exception, ?ServerRequestInterface $request = null, ?int $responseStatusCode = null): void
	{
		$projectName = ReportErrorsServiceClient::projectName($this->googleProjectId);

		$reportLocation = new SourceLocation();
		$reportLocation->setFunctionName(self::getFunctionNameForReport($exception->getTrace()));
		$reportLocation->setLineNumber($exception->getLine());
		$reportLocation->setFilePath($exception->getFile());

		$context = new ErrorContext();
		$context->setReportLocation($reportLocation);

		if ($request !== null) {
			$requestContext = new HttpRequestContext();
			$requestContext->setMethod($request->getMethod());
			$requestContext->setUrl((string) $request->getUri());

			if ($responseStatusCode !== null) {
				$requestContext->setResponseStatusCode($responseStatusCode);
			}

			$userAgent = $request->getHeaderLine('user-agent');
			if ($userAgent !== '') {
				$requestContext->setUserAgent($userAgent);
			}

			$referer = $request->getHeaderLine('referer');
			if ($referer !== '') {
				$requestContext->setReferrer($referer);
			}

			$context->setHttpRequest($requestContext);
		}

		$serviceContext = new ServiceContext();
		$serviceContext->setService($this->projectName);

		if ($this->projectVersion !== null) {
			$serviceContext->setVersion($this->projectVersion);
		}

		$errorType = $exception instanceof \ParseError ?
			'Parse error' :
			'Fatal error';

		$event = new ReportedErrorEvent();
		$event->setMessage(\sprintf('PHP %s: %s', $errorType, (string) $exception));
		$event->setContext($context);
		$event->setServiceContext($serviceContext);

		$this->client->reportErrorEvent($projectName, $event);
	}

	/**
	 * Taken from google/error-reporting
	 * @param mixed[] $trace
	 * @return string
	 */
	private function getFunctionNameForReport(array $trace): string
	{
		if ($trace[0]['function'] === '') {
			return '<none>';
		}

		$functionName = [$trace[0]['function']];

		if (isset($trace[0]['type'])) {
			$functionName[] = $trace[0]['type'];
		}

		if (isset($trace[0]['class'])) {
			$functionName[] = $trace[0]['class'];
		}

		return \implode('', \array_reverse($functionName));
	}
}
