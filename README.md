# DKX\GoogleReportErrorExceptionWriter

Wrapper for Google ErrorReporting library

## Installation

```bash
$ composer require dkx/google-report-error-exception-writer
```

## Usage

```php
<?php

namespace MyApp;

use DKX\GoogleReportErrorExceptionWriter\ReportErrorExceptionWriter;
use Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient;

$googleProjectId = 'google-project-id';
$projectName = 'my-app/production';
$projectVersion = '0.0.1';

$client = new ReportErrorsServiceClient();
$writer = new ReportErrorExceptionWriter($client, $googleProjectId, $projectName, $projectVersion);

try {
    buggy_code();
} catch (\Throwable $e) {
    $writer->writeException($e);
}
```

Current HTTP request can be stored in the error context if you use PSR7 for HTTP:

```php
<?php

$responseStatusCode = 500;
$writer->writeException($e, $httpRequest, $responseStatusCode);
``` 
